# GrandCore

*Первая в Мире Open Source корпорация. Универсальная платформа для любых открытых проектов*

- Задачи в Trello - https://trello.com/b/K73NeI1B/grandcore-front-react
- **Рабочий чат фронтендеров - https://t.me/joinchat/OQ6DaxmA5Xf7GERw8oZO8g** 
- Рабочий чат бекендеров -  https://t.me/joinchat/OQ6Da0w3cCxnfGl9K2Tv9g

- Наш сайт - https://grandcore.org
- Наши новости - https://t.me/grandcore_news
- Публичный чат - https://t.me/grandcore_chat
- Написать основателю - https://t.me/grandcore  **(если хотите в команду)**

- Текущие макеты в Figma - https://www.figma.com/file/NlikNEJQHliYlxI3MHhiSW/Share?node-id=2946%3A1036
- Репозиторий с компонентами и вёрсткой - https://gitlab.com/grandcore/stroika
- Репозиторий бекенда на Python - https://gitlab.com/grandcore/grandcore-backend-python


Вёрстка и API для MVP на бекенде уже готовы!

## Быстрый старт

1. Клонируйте репозиторий:
  ```shell
  $ git clone url.git
  ```

2. Установите зависимости:

  ```shell
  $ npm install
  ```

  *We recommend using `npm` for installing packages, but you can use `yarn` instead*:

  ```shell
  $ yarn install
  ```
3. Запуск локального сервера

  ```shell
  $ npm start
  ```

4. Сборка проекта
``` shell
$ npm run build
```

## Features

* Redux
* Modern ES6 for using template strings, JSX syntax, object destructuring arrow functions and more
* Babel for old browser support
* TypeScript
* SASS/SCSS: make styles greate again, with no tears
* React Router
* Hot Module Replacement for comfortable development

## Command Line Commands

### Install
```shell
$ npm install
```

### Start dev server
```shell
$ npm start
```

### Build
```shell
$ npm run build
```

## Структура проекта
#### src/
Корень react-приложения
#### src/components
Компоненты
#### src/media
Медиа-ресурсы (*svg, *.png, etc)
#### src/routes
Роуты приложения
#### src/views
View-модели приложения
#### src/styles
Основные стили приложения