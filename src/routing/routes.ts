import { IRoute } from 'types/Route'

export const routes: Array<IRoute> = [
  {
    path: '/auth',
    text: 'Auth Page'
  }
]
