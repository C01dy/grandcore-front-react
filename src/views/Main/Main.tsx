import React, { FC } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import Auth from 'views/Auth/Auth'
import About from '../About/About'
import Header from 'components/Header/Header'
import styles from 'views/Main/Main.module.scss'

interface IProps {}

const Main: FC<IProps> = () => {
  return <Router>
    <Header />

    <main className={styles.main}>
      <Switch>
        <Route path='/' exact>
          <Redirect to='/about' />
        </Route>

        <Route path='/auth'>
          <Auth />
        </Route>

        <Route path='/about'>
          <About />
        </Route>
      </Switch>
    </main>
  </Router>
}

export default Main
