import React, { FC } from 'react'
import NavbarLinks from 'components/Navbar/NavbarLinks'
import styles from 'components/Navbar/Navbar.module.scss'
import { routes } from 'routing/routes'

interface IProps {}

const Navbar: FC<IProps> = () => {
  return <nav className={styles.navbar}>
    <NavbarLinks routes={routes} />
  </nav>
}

export default Navbar
