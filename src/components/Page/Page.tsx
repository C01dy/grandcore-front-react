import React, { FC } from 'react'
import styles from 'components/Page/Page.module.scss'

interface IProps {}

const Page: FC<IProps> = props => {
  return <section className={styles.page} children={props.children} />
}

export default Page
