import React, { FC } from 'react'
import { IRoute } from 'types/Route'
import NavbarLink from 'components/Navbar/NavbarLink'
import styles from 'components/Navbar/Navbar.module.scss'

interface IProps {
  routes: Array<IRoute>
}

const NavbarLinks: FC<IProps> = ({ routes = [] }) => {
  return <ul className={styles.navbar__list}>
    {routes.map((route, index) => <NavbarLink route={route} key={index} />)}
  </ul>
}

export default NavbarLinks
