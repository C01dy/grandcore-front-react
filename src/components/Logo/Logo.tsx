import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { ReactComponent as LogoImage } from 'media/logo.svg'
import { ReactComponent as LogoText } from 'media/logo-text.svg'
import styles from 'components/Logo/Logo.module.scss'

interface IProps {}

const Logo: FC<IProps> = () => {
  return <Link to='/'>
    <div className={styles.logo}>
      <LogoImage />

      <LogoText />
    </div>
  </Link>
}

export default Logo
